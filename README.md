# Canvas Panel Hanger

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)


# Modules

## canvas-panel-hanger


# License

## Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International

Copyright © 2023 Andrew A. Tasso

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International 
(CC BY-NC-SA 4.0) License. The full terms of the license can be found in [LICENSE.txt](LICENSE.txt) in the root of this 
project or at http://creativecommons.org/licenses/by-nc-sa/4.0/.
