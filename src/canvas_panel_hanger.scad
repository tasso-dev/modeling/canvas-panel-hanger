/*
* Canvas Panel Hanger (c) by Andrew A. Tasso
* 
* Canvas Panel Hanger is licensed under a Creative Commons 
* Attribution-NonCommercial-ShareAlike 4.0 International License.
* 
* You should have received a copy of the license along with this
* work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.
*/


// keep the number of fragments low while in preview to improve performance
$fn = $preview ? 20 : 100;
eps = 0.001;


canvas_panel_hanger();


module canvas_panel_hanger() {

}
